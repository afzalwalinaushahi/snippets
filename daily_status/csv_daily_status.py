import csv
import os
from datetime import datetime, timedelta
import re

def get_due_in_business_days(due_date_actual):
    # This function returns the number of business days >0 for future dates and <0 for past
    # The variables working_time_start, working_time_end etc can be modified according to your own preferences
    # These help determine the business day due time. Suppose a ticket is due at 4 AM on 10th which is a Monday, then
    # it was actually supposed to be completed by end of day (e.g. 6pm) on Friday. This function takes care of weekends
    # and the working_time_end etc. when calculating the business days.

    working_time_start = 13
    working_time_mid = 15  # due before 3 pm means due yesterdays 6 pm.
    working_time_near_end = 19  # due after 7 pm means due at 7 pm.
    working_time_end = 22

    hour = due_date_actual.hour
    due_date_practical = due_date_actual
    time_now = datetime.now()

    if 0 <= hour < working_time_mid:
        due_date_practical = due_date_actual.replace(hour=18, minute=0) - timedelta(1)
    elif working_time_near_end < hour <= 23:
        due_date_practical = due_date_actual.replace(hour=18, minute=0)

    weekday = due_date_practical.weekday()  # M=0;T=1;W=2;T=3;F=4;S=5;S=6
    if weekday == 6:
        due_date_practical = due_date_practical - timedelta(2)
    elif weekday == 5:
        due_date_practical = due_date_practical - timedelta(1)

    next_working_time = time_now
    if next_working_time.weekday() == 5:
        next_working_time = next_working_time + timedelta(2)
        next_working_time = next_working_time.replace(hour=working_time_start, minute=0, second=0)
    elif next_working_time.weekday() == 6:
        next_working_time = next_working_time + timedelta(1)
        next_working_time = next_working_time.replace(hour=working_time_start, minute=0, second=0)

    if next_working_time.hour < working_time_start:
        next_working_time = next_working_time.replace(hour=working_time_start)
    elif next_working_time.hour > working_time_end:
        next_working_time = next_working_time + timedelta(1)
        if next_working_time.weekday() == 5:
            next_working_time = next_working_time + timedelta(2)
            next_working_time = next_working_time.replace(hour=working_time_start, minute=0, second=0)
        elif next_working_time.weekday() == 6:
            next_working_time = next_working_time + timedelta(1)
            next_working_time = next_working_time.replace(hour=working_time_start, minute=0, second=0)

    if due_date_practical > next_working_time:
        fromdate = next_working_time
        todate = due_date_practical
        daygenerator = (fromdate + timedelta(x + 1) for x in range((todate - fromdate).days))
        days = sum(1 for day in daygenerator if day.weekday() < 5)
        if days == 0:
            diff_seconds = (due_date_practical - next_working_time).total_seconds()
            return diff_seconds / 60 / 60 / 24.0
        else:
            return days

    else:
        todate = next_working_time
        fromdate = due_date_practical
        daygenerator = (fromdate + timedelta(x + 1) for x in range((todate - fromdate).days))
        days = sum(1 for day in daygenerator if day.weekday() < 5)
        if days == 0:
            diff_seconds = (next_working_time - due_date_practical).total_seconds()
            return -1 * diff_seconds / 60 / 60 / 24.0
        else:
            return days * -1


def formatted_date(date_obj):
    # This returns  a formatted string e.g. "1 h ago", or "in 2 days" etc.
    due_in_business_days = get_due_in_business_days(date_obj)

    if due_in_business_days < 0:
        if due_in_business_days <= -1:
            return '%s(%02dd ago)' % (date_obj.strftime('%m/%d'), due_in_business_days * -1)
        else:
            return '%s(%02dh ago)' % (date_obj.strftime('%m/%d'), int(due_in_business_days * -1 * 24))
    else:
        if due_in_business_days >= 1:
            return '%s(in  %02dd)' % (date_obj.strftime('%m/%d'), due_in_business_days)
        else:
            # return '%s(in     %02dh)'%(date_obj.strftime('%m/%d'), hours)
            return '%s(in  %02dh)' % (date_obj.strftime('%m/%d'), int(due_in_business_days * 24))


def formatted_ticket_line(t, is_pending=False):
    # Generates a complete string for each line of the ticket.

    is_comm_late = t[next_communication_label] < (datetime.now() + timedelta(hours=5))
    is_last_update_late = t[last_update_time_label] < (datetime.now() - timedelta(hours=50))
    is_due_late = t[due_by_time_label] < (datetime.now() + timedelta(days=3))
    is_pending_crawl = t[status_label] in ['Crawl in Progress']
    is_pending_customer = t[status_label] in ['Pending - Customer']
    is_customer_side_ticket = t[origin_of_ticket_label] in ['Customer']

    is_ticket_assigned = False
    comm_only = False
    due_only = False
    due_comm = False
    not_updated = False
    if is_pending_customer:
        if (is_last_update_late and not is_due_late) or (is_last_update_late and is_due_late):
            if not is_ticket_assigned:
                comm_only = True
                is_ticket_assigned = True
    else:
        if (is_comm_late and not is_due_late) or (is_comm_late and is_due_late and is_pending_crawl):
            if not is_ticket_assigned:
                comm_only = True
                is_ticket_assigned = True

    if (is_due_late and not is_comm_late and not (is_pending_crawl or is_pending_customer)):
        if not is_ticket_assigned:
            due_only = True
            is_ticket_assigned = True

    if is_comm_late and is_due_late and not (is_pending_crawl or is_pending_customer):
        if not is_ticket_assigned:
            due_comm = True
            is_ticket_assigned = True

    if t[last_update_time_label] < (datetime.now() + timedelta(days=-3)):
        if not is_ticket_assigned:
            not_updated = True
            is_ticket_assigned = True

    prefix = ''
    suffix = ''
    if comm_only:
        suffix += 'C'
    if due_only:
        suffix += 'D'
    if due_comm:
        suffix += 'D+C'
    if not_updated:
        suffix += 'U'

    if suffix:
        suffix = '[' + suffix + ']'

    if is_customer_side_ticket:
        separator = '*C-'
    else:
        separator = ' - '

    if is_pending:
        line = f'{t[ticket_id_label]}{separator}{(t[subject_label]+ " "*100)[0:50]};{t[priority_label][0:3].upper()}; Due:{formatted_date(t[due_by_time_label])}; LUp:{formatted_date(t[last_update_time_label])}'
    else:
        line = f'{t[ticket_id_label]}{separator}{(t[subject_label]+ " "*100)[0:50]};{t[priority_label][0:3].upper()}; Due:{formatted_date(t[due_by_time_label])}; NCm:{formatted_date(t[next_communication_label])}{suffix}'
    return line


def move_files_from_downloads():
    source_folder = os.path.expanduser("~") + "/Downloads/"
    dest_folder = os.path.dirname(os.path.abspath(__file__))

    files_in_dir = os.listdir(source_folder)
    list_of_files = []
    for file in files_in_dir:
        search_obj = re.search('\\d{11}_tickets.{5,10}\\d{4}\\-\\d\\d[_\\-]\\d\\d.csv', file)
        if search_obj:
            list_of_files.append(file)

    if len(list_of_files) == 0:
        files_in_dir = os.listdir(dest_folder)
        list_of_files = []
        for file in files_in_dir:
            search_obj = re.search('\\d{11}_tickets.{5,10}\\d{4}\\-\\d\\d[_\\-]\\d\\d.csv', file)
            if search_obj:
                list_of_files.append(file)

        file = list_of_files[-1]
        # print (os.path.join(dest_folder, file))
        return os.path.join(dest_folder, file)

    elif len(list_of_files) > 1:
        print("Expecting 1 file. Found %s file(s)" % len(list_of_files))
        exit(1)
    else:
        file = list_of_files[0]
        os.rename(os.path.join(source_folder, file), os.path.join(dest_folder, file))
        print(os.path.join(dest_folder, file))
        return os.path.join(dest_folder, file)


def print_alert_message(msg, col_width=100):
    if (col_width % 2) != 0:
        col_width += 1

    msg = '  ' + msg + '  '
    if (len(msg) % 2) != 0:
        msg = msg + ' '

    print('*' * col_width)
    print('*' * (int((col_width - len(msg)) / 2)) + msg + '*' * (int((col_width - len(msg)) / 2)))
    print('*' * col_width)


if __name__ == "__main__":

    ticket_id_label = 'Ticket ID'
    subject_label = 'Subject'
    status_label = 'Status'
    priority_label = 'Priority'
    due_by_time_label = 'Due by Time'
    last_update_time_label = 'Last update time'
    origin_of_ticket_label = 'Origin of the ticket'
    next_communication_label = 'Next communication'
    due_next_3_days = 0
    due_next_5_days = 0
    due_next_10_days = 0

    file = move_files_from_downloads()  # May-28-2020-09-12

    file_date_str = file.split('.')[0].split('_tickets-')[1]
    file_date = datetime.strptime(file_date_str, "%B-%d-%Y-%H-%M")
    data_is_fresh = (datetime.utcnow() - file_date).total_seconds() < (30 * 60)

    required_cols = [ticket_id_label, subject_label, status_label, priority_label, due_by_time_label,
                     last_update_time_label, origin_of_ticket_label, next_communication_label]
    required_col_positions = {}
    all_tickets = []

    lookup_dict = {}
    with open(file, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for cols in reader:
            if cols[0] in required_cols:
                for required_col in required_cols:
                    required_col_positions[required_col] = cols.index(required_col)
                continue

            ticket_id = cols[required_col_positions[ticket_id_label]]
            subject = cols[required_col_positions[subject_label]]
            status = cols[required_col_positions[status_label]]
            priority = cols[required_col_positions[priority_label]]
            origin_of_ticket = cols[required_col_positions[origin_of_ticket_label]]

            last_update_time_str = cols[required_col_positions[last_update_time_label]]
            last_update_time = datetime.strptime(last_update_time_str, "%Y-%m-%d %H:%M:%S")

            next_communication_str = cols[required_col_positions[next_communication_label]]
            if next_communication_str:
                next_communication = datetime.strptime(next_communication_str, "%d %b, %Y")
                next_communication = next_communication.replace(hour=18)
            else:
                next_communication = datetime.now() + timedelta(2 / 24)

            due_by_time_str = cols[required_col_positions[due_by_time_label]]
            due_by_time = datetime.strptime(due_by_time_str, "%Y-%m-%d %H:%M:%S")

            due_in_business_days = get_due_in_business_days(due_by_time)

            if due_in_business_days <= 3:
                if status not in ['Crawl in Progress', 'Pending - Customer']:
                    due_next_3_days += 1
                    due_next_5_days += 1
                else:
                    due_next_3_days += 0.5
                    due_next_5_days += 0.5
                due_next_10_days += 1
            elif due_in_business_days <= 5:
                due_next_5_days += 1
                due_next_10_days += 1
            else:
                due_next_10_days += 1

            all_tickets.append({
                ticket_id_label: ticket_id,
                subject_label: subject,
                status_label: status,
                priority_label: priority,
                last_update_time_label: last_update_time,
                origin_of_ticket_label: origin_of_ticket,
                next_communication_label: next_communication,
                due_by_time_label: due_by_time
            })

    in_progress = []
    open_tickets = []
    crawl_in_progress = []
    pending_customer = []

    for t in all_tickets:
        if t[status_label] not in ['Crawl in Progress', 'Pending - Customer']:
                if t[status_label] in ['Open']:
                    open_tickets.append(t)
                else:
                    in_progress.append(t)

    for t in all_tickets:
        if t[status_label] in ['Crawl in Progress']:
            crawl_in_progress.append(t)
    for t in all_tickets:
        if t[status_label] in ['Pending - Customer']:
            pending_customer.append(t)

    if not data_is_fresh:
        print_alert_message('DATA NOT FRESH', 102)
    else:
        print('Data is %d minutes old.' % (((datetime.utcnow() - file_date).total_seconds()) / 60))

    print('*Work in Progress*')
    new_list = sorted(in_progress, key=lambda i: (i[due_by_time_label], i[next_communication_label]))
    for t in new_list:
        print(formatted_ticket_line(t))
    print('*Crawl in Progress*')
    new_list = sorted(crawl_in_progress, key=lambda i: (i[due_by_time_label], i[next_communication_label]))
    for t in new_list:
        print(formatted_ticket_line(t))
    print('*In QA*')
    print('*In Code Review*')
    print('*Open*')
    new_list = sorted(open_tickets, key=lambda i: (i[due_by_time_label], i[next_communication_label]))
    for t in new_list:
        print(formatted_ticket_line(t))
    print('*Pending - Customer*')
    new_list = sorted(pending_customer, key=lambda i: i[last_update_time_label], reverse=False)
    for t in new_list:
        print(formatted_ticket_line(t, is_pending=True))

    min3 = 2;max3 = 3
    min5 = 4;max5 = 6
    min10 = 7;max10 = 9

    print('Workload:')
    print('  Tickets Due: in 3d: %d; in 5d: %d; in 10d: %d' % (due_next_3_days, due_next_5_days, due_next_10_days))

    if due_next_3_days > max3 or due_next_5_days > max5:
        print('  Today: Overloaded')
    elif due_next_5_days < min3 or due_next_10_days < min5:
        print('  Today: Need More Work')
    else:
        print('  Today: Normal')

    if due_next_5_days > max5 or due_next_10_days > max10:
        print('  Next 2 days: Overloaded')
    elif due_next_5_days < min3 or due_next_10_days < min5:
        print('  Next 2 days: Need more work')
    else:
        print('  Next 2 days: Normal')

    if not data_is_fresh:
        print_alert_message('DATA NOT FRESH', 102)
